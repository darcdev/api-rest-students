<?php
require_once('./lib/connect.php');

/**
 * Realize call to libraries or services to retrieve student information
 */
class StudentsService {
    private $entity;
    private $pgsql;
    public function __construct($config) {
        $this->entity = 'students';
        $this->pgsql = new PgSqlLib($config);
    }
    /** 
     * connect to database & get all students
     */
    public function getStudents($query){
        $connect =  $this->pgsql;
        $students = $connect->getAll($this->entity , $query);
        return $students;
    }
}
?>