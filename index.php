<?php
  require_once('./config/env.php');
  require_once('./services/students.php');
  
  //call to student service , pass (config database)
  $studentService = new StudentsService($config);
  $response = $studentService->getStudents("SELECT * FROM students");
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="./public/assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="./public/assets/css/styles.css">
      <title>Estudiantes</title>
   </head>
   <body>
      <div class="container">
         <div class="row">
            <div class="col-12">
               <h1 class="text-center mt-4">Lista de Estudiantes</h1>
            </div>
            <div class="col-12 mt-4">
               <h4>Crear nuevo estudiante</h4>
               <button type="button" class="btn btn-success">Nuevo</button>
            </div>
            <div class="col-12 mt-4">
               <table class="table table-striped table-hover">
                  <thead class="thead-dark">
                     <tr>
                        <th scope="col">#</th>
                        <th scope="col">Codigo</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Edad</th>
                        <th scope="col">Semestre</th>
                        <th scope="col"></th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php foreach($response as $i=>$student){?>
                        <tr>
                           <th scope="row"><?php echo $i ?></th>
                           <td><?php echo $student['student_id'] ?></td>
                           <td><?php echo $student['name'] ?></td>
                           <td><?php echo $student['age'] ?></td>
                           <td><?php echo $student['semester'] ?></td>
                           <td><button type="button" class="btn btn-primary">Editar</button>
                           <button type="button" class="btn btn-danger">Eliminar</button>
                           </td></tr>
                     <?php } ?>
                  </tbody>
               </table>
            </div>
       
         </div>
      </div>
   </body>
</html>