# API REST STUDENTS 
 Create Crud to students information 
 
## Preview 

Retrieve student information

![ListStudents](https://gitlab.com/darcdev/api-rest-students/-/raw/master/gh-images/list-students.png)

### Basic Operations 
  - Read
  - Write
  - Update
  - Delete
### Database Connection

```php
<?php
public function connect (){
 try{    
    $pgsqlConnect = new PDO(''.$this->db.':'.'host='.$this->dbHost.';dbname='.$this->dbName.'',$this->dbUser, $this->dbPassword) or die ("Error de Conexion".pg_last_error());
    }catch(Exception $e){
        echo "Error" + $e;
    }
    return $pgsqlConnect;
}
 ?>
```

### Terminal Commands 
  - Insert students
  ![insertStudent](https://gitlab.com/darcdev/api-rest-students/-/raw/master/gh-images/terminal-insert.png)
### Todos
  - implement other methods
  - create classes by entity
### LICENSE
----
MIT