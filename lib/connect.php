<?php
/**
 * interact with postgress database
 */
class PgSqlLib {

    public $pgsqlConnect;
    public $db;
    public $dbHost;
    public $dbName;
    public $dbUser;
    public $dbPassword;

    public function __construct($config){
      $this->db = $config -> db;
      $this->dbHost = $config -> dbHost;
      $this->dbName = $config -> dbName;
      $this->dbUser =  $config -> dbUser;
      $this->dbPassword = $config -> dbPassword;
    }
    /** 
     * Create connection to postgres database
     */
    public function connect (){
        try{    
                $pgsqlConnect = new PDO(''.$this->db.':'.'host='.$this->dbHost.';dbname='.$this->dbName.'',$this->dbUser, $this->dbPassword) or die ("Error connection".pg_last_error());
            }catch(Exception $e){
                echo "Error" + $e;
            }
        return $pgsqlConnect;
    }
    /**
     * get all to determinate entity
     */
    public function getAll($entity , $query){
        $connect = $this->connect();
        $queryRespose = $connect->query($query);
        return $queryRespose;
    }
}
?>